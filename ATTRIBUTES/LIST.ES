
/*
	var ATTRIBUTES = require ("AURA/ATTRIBUTES/LIST") (ELEMENT);
*/
module.exports = function (ELEMENT) {
	var RESULT = {};

	var A = ELEMENT.attributes;
	for (let Z = 0; Z < A.length; Z++) {
		const ATTRIBUTE = A [ Z ];

		RESULT [ ATTRIBUTE.name ] = ATTRIBUTE.value;
	}

	return RESULT;
}
