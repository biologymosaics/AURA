
/*
	HOPES:

		var { JEWEL } = require ("AURA/JEWEL") ({

		});
*/

var CREATE_CRYSTAL = require ("../CRYSTALS/CREATE.ES");
var RANDOM = require ("RANDOM");
var RANDOM_INTEGER = require ("RANDOM/INTEGER");

function BACKGROUND () {
	var COLORS = [
		`red`,
		`green`,
		`violet`,
		`yellow`,
		`indigo`,
		`orange`,
		`blue`
	];
	require ("SQUIGLES/ARRAY/SHUFFLE") (COLORS);

	var ANGLE = `${ RANDOM_INTEGER (1, 360) }deg`;
	var BACKGROUND = `linear-gradient(${ ANGLE },${ COLORS.join (",") })`;

	return BACKGROUND;
}

module.exports = function ({
	STYLES = {}
} = {}) {
	var CRATE = CREATE_CRYSTAL ("div", {
		STYLES: Object.assign ({}, {
			position: "relative",
			overflow: "hidden",
			height: "100%",
			width: "100%",

			overflow: "hidden",
			boxShadow: "0 0 3px 3px #f4e8a8 inset",
			borderRadius: "18px"
		}, STYLES)
	});

	var OVERLAY_I = CREATE_CRYSTAL ("canvas", {
		STYLES: {
			position: "absolute",
			height: "100%",
			width: "100%",
			background: BACKGROUND (),
			opacity: RANDOM (.4, .5),
		}
	});
	var OVERLAY_II = CREATE_CRYSTAL ("canvas", {
		STYLES: {
			position: "absolute",
			transform: [
				`rotateZ(${ RANDOM (4, 12) }deg)`,
				`rotateX(${ RANDOM (6, 32) }deg)`,
				`skew(${ RANDOM (12, 25) }deg)`,
				`scale(${
					RANDOM (1.4, 1.6)
				}, ${
					RANDOM (.8, .9)
				})`
			].join (" "),
			height: "100%",
			width: "100%",
			background: BACKGROUND (),
			opacity: RANDOM (.2, .6),
			borderRadius: ".96in"
		}
	});
	var OVERLAY_III = CREATE_CRYSTAL ("canvas", {
		STYLES: {
			position: "absolute",
			left: `${ RANDOM_INTEGER (-3, -21) }%`,
			top: `${ RANDOM_INTEGER (4, 12) }%`,

			transform: [
				`rotateZ(${ RANDOM (-30, 4) }deg)`,
				`rotateY(${ RANDOM (4, 49) }deg)`,
				`skew(${ RANDOM (-12, -25) }deg)`,
				`scale(${
					RANDOM (1.9, 2.2)
				}, ${
					RANDOM (.7, .8)
				})`
			].join (" "),

			height: "100%",
			width: "100%",
			background: BACKGROUND (),
			opacity: RANDOM (.2, .4),
			borderRadius: ".34in"
		}
	});

	var OVERLAY_IV = CREATE_CRYSTAL ("canvas", {
		STYLES: {
			position: "absolute",
			left: `${ RANDOM_INTEGER (3, 6) }%`,
			transform: [
				`rotateZ(${ RANDOM (39, 272) }deg)`,
				`rotateY(${ RANDOM (14, 51) }deg)`,
				`rotateX(${ RANDOM (-58, -19) }deg)`,
				`skew(${ RANDOM (28, 54) }deg)`,
				`scale(${
					RANDOM (2.1, 2.3)
				}, ${
					RANDOM (1.8, 1.9)
				})`
			].join (" "),
			height: "100%",
			width: "100%",
			background: BACKGROUND (),
			opacity: RANDOM (.2, .3),
			borderRadius: ".26in"
		}
	});

	CRATE.appendChild (OVERLAY_I);
	CRATE.appendChild (OVERLAY_II);
	CRATE.appendChild (OVERLAY_III);
	CRATE.appendChild (OVERLAY_IV);

	return {
		JEWEL: CRATE
	}
}
















/////////////////////
