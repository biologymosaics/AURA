

/*
	var SECTION = document.getElementById ("SECTION");
	// THIS NEEDS TO HAVE A TABINDEX ^


	var { SVG, DISCONNECT } = require ("AURA/HIGHLIGHT") ({
		GRADIENT_ID: "FOCUS_GRADIENT",
		ELEMENT: SECTION
	});

	SECTION.addEventListener ("focus", function () {
		var SHOW_ANIMATION = require ("animejs").default ({
			targets: SVG,
			opacity: .8,
			duration: 500,
			easing: 'easeInOutQuad',
			complete: function(anim) {
				// console.log ('completed : ' + anim.completed);
			}
		});
	});

	SECTION.addEventListener ("blur", function () {
		require ("animejs").default ({
			targets: SVG,
			opacity: 0,
			duration: 500,
			easing: 'easeInOutQuad',
			complete: function (anim) {
				// console.log ('completed : ' + anim.completed);
			}
		});
	});
*/


var ResizeObserver = require ("@juggle/resize-observer").ResizeObserver;

var GRADIENT = require ("./AUX/GRADIENT.ES");
var CREATE_NS = require ("../CRYSTALS/CREATE_NS.ES");

module.exports = function ({
	GRADIENT_ID,
	ELEMENT
}) {
	var CRATE = ELEMENT.getBoundingClientRect ();

	var DIMENSION = 12;

	var SVG = CREATE_NS ("svg", {
		NS_ATTRIBUTES: {
			version: "1.1",

			width: `calc(100% + ${ DIMENSION * 2 }px)`,
			height: `calc(100% + ${ DIMENSION * 2 }px)`,
		},
		STYLES: {
			position: "absolute",
			top: -DIMENSION + "px",
			left: -DIMENSION + "px",

			pointerEvents: "none",
			opacity: 0
		}
	});

	var { DEFS } = GRADIENT ({
		GRADIENT_ID
	});
	SVG.appendChild (DEFS);


	var RECT = CREATE_NS ("rect", {
		NS_ATTRIBUTES: {
			x: 0 + (DIMENSION / 2),
			y: 0 + (DIMENSION / 2),

			rx: 11,
			ry: 11,

			width: `100%`,
			height: `100%`,

			stroke: `url(#${ GRADIENT_ID })`,

			["stroke-width"]: DIMENSION + 1,
			["stroke-opacity"]: .7,
			["fill-opacity"]: 0,

		},
		STYLES: {
			pointerEvents: "none"
		}
	});

	var _observer;
	const ro = new ResizeObserver ((entries, observer) => {
		_observer = observer;

		var CRATE = ELEMENT.getBoundingClientRect ();
		RECT.setAttributeNS (null, "width", CRATE.width + DIMENSION);
		RECT.setAttributeNS (null, "height", CRATE.height + DIMENSION);
	});

	ro.observe (ELEMENT); // Watch dimension changes on body

	SVG.appendChild (RECT);
	ELEMENT.appendChild (SVG);

	return {
		SVG,
		DISCONNECT: () => {
			try {
				_observer.disconnect ()
			}
			catch (T) {
				console.error (T);
			}
		}
	}
};
