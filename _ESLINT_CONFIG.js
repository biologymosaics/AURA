
/*


*/

// module.exports = {
//     "env": {
//         // "browser": true,
//         // "es2021": true,
//         "node": true
//     },
//     "extends": "eslint:recommended",
//     "parserOptions": {
//         "ecmaVersion": 12
//     },
//     "rules": {
// 		"no-mixed-spaces-and-tabs": "off"
//     }
// };
//

module.exports = {
	"ignorePatterns": [
		"__CHECKS__/**/*",

		"**/*.BUNDLE.ES",
		"**/*.CYPRESS.ES",
		"**/*.MOCHA.ES",

		".eslintrc.js",
		"KARMA.conf.js"
	],
	"env": {
		"browser": true,
		"commonjs": true
	},
	"extends": "eslint:recommended",
	"parserOptions": {
		"ecmaVersion": 12
	},
	"rules": {
		"no-mixed-spaces-and-tabs": "off",
		"no-constant-condition": "off",

		"no-async-promise-executor": "warn",
		"no-prototype-builtins": "warn",
		"no-undef": "warn",
		"no-unreachable": "warn",
		"no-unused-vars": "warn"
	}
};
