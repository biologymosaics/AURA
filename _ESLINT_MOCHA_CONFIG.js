
/*


*/

// module.exports = {
//     "env": {
//         // "browser": true,
//         // "es2021": true,
//         "node": true
//     },
//     "extends": "eslint:recommended",
//     "parserOptions": {
//         "ecmaVersion": 12
//     },
//     "rules": {
// 		"no-mixed-spaces-and-tabs": "off"
//     }
// };
//

module.exports = {
	"ignorePatterns": [
		// "__CHECKS__/**/*",
		// "**/*.MOCHA.ES",
		".eslintrc.js",
		"KARMA.conf.js"
	],
	"env": {
		"browser": true,
		"commonjs": true,
		"mocha": true
	},
	"extends": "eslint:recommended",
	"parserOptions": {
		"ecmaVersion": 12
	},
	"rules": {
		"no-mixed-spaces-and-tabs": "off",
		"no-async-promise-executor": "warn",
		"no-unused-vars": "warn",
		"no-prototype-builtins": "warn",
		"no-undef": "warn"
	}
};
