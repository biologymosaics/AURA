
/*
	FOCUS NEXT TAB AFTER ELEMENT

		AFTER NEEDS TO BE AN ELEMENT WITH A TABINDEX

	var NEXT = require ("AURA/FOCUS/GET-NEXT-TABINDEX") ({
		AFTER
	});



	ASPIRATIONS
		INCLUDE
			"a"
			"button"
			document.body

*/
module.exports = function ({
	AFTER
}) {
	var INDEXES = document.querySelectorAll ('body, [tabindex], a, button');

	var INDEX_OF_AFTER;
	var AFTER_FOUND = false;
	for (let A = 0; A < INDEXES.length; A++) {
		if (INDEXES [ A ] === AFTER) {
			INDEX_OF_AFTER = A;
			AFTER_FOUND = true;
		}
	}

	var A = INDEX_OF_AFTER;


	// var NEXT_TAB_INDEX;
	if (AFTER_FOUND) {
		while (true) {
			// IF A IS AT THE END OF THE LOOP, GOTO THE BEGINNING OF THE LOOP
			if (A === INDEXES.length - 1) {
				A = 0;
			}
			else {
				A++;
			}

			/*
				If doesn't exist in tree or document??
				this might not be necessary????
			*/
			if (INDEXES [ A ].offsetParent === null) {
				if (INDEXES [ A ] !== document.body) {
					continue;
				}
			}

			/*
				CHECK TO MAKE SURE display is not none
			*/
			if (INDEXES [ A ].style.display === "none") {
				continue;
			}

			// IF A FULL LOOP OCCURED, DO NOTHING... ?????
			if (A === INDEX_OF_AFTER) {
				return null;
			}

			return INDEXES [ A ];
		}

		return null;
	}

	return null;
}






////////
