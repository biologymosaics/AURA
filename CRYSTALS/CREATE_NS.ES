
/*
	require ("AURA/CRYSTALS/CREATE_NS.ES") ("div", {
		TEXT:
		NS_ATTRIBUTES: {
			version: "1.1"
			
		},
		STYLES: {

		},
		EVENTS: {},
		CHILDREN: []
	});
*/

module.exports = function (NAME, DETAILS) {
	var NS = "http://www.w3.org/2000/svg";

	if (typeof DETAILS.NS === "string") {
		NS = DETAILS.NS;
	}

	var ELEMENT = document.createElementNS (
		NS,
		NAME
	);

	if (typeof DETAILS === "object") {
		if (DETAILS.NS_ATTRIBUTES) {
			require ("./../NS_ATTRIBUTES/CREATE.ES") (
				ELEMENT,
				DETAILS.NS_ATTRIBUTES
			);
		}

		if (DETAILS.STYLES) {
			require ("./../STYLES/CREATE.ES") (ELEMENT, DETAILS.STYLES);
		}

		if (DETAILS.EVENTS) {
			require ("./../EVENTS/CREATE.ES") (ELEMENT, DETAILS.EVENTS);
		}

		if (DETAILS.CHILDREN) {
			DETAILS.CHILDREN.forEach (C => {
				ELEMENT.appendChild (C);
			});
		}
	}

	return ELEMENT;
}
