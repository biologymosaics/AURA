


module.exports = {
	"temp-directory": "./__CHECKS__/COVERAGE",
	"report-dir": "./__CHECKS__/COVERAGE",
	
	"all": true,
	"extension": [ ".ES", ".js" ],
	"include": [
		"**/*.JS",
		"**/*.ES",
		"**/*.es",
		"**/*.js"
	],
	"exclude": []
}
